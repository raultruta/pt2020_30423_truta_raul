package Project1.NewDemoProject;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import nomen.Polynomial;
import operations.*;

public class App extends JFrame {

	JPanel panel = new JPanel();
	JButton addButton = new JButton("+");
	JButton subButton = new JButton("-");
	JButton multiButton = new JButton("*");
	JButton divButton = new JButton("/");
	JButton derivButton = new JButton("'");
	JButton integButton = new JButton("S");
	JLabel t1 = new JLabel("Please write your polynomials:");
	JTextField tf1 = new JTextField(17);
	JTextField tf2 = new JTextField(17);
	JLabel t2 = new JLabel("Result:");
	JTextField tf3 = new JTextField(30);
	JTextField tf4 = new JTextField(30);

	public App() {

		super("App");
		setSize(730, 400);
		setResizable(true);
		setVisible(true);
		panel.setLayout(new GridLayout(0, 2));
		panel.add(t1);
		panel.add(new JLabel());
		panel.add(tf1);
		panel.add(tf2);
		panel.add(addButton);
		panel.add(subButton);
		panel.add(multiButton);
		panel.add(divButton);
		panel.add(derivButton);
		panel.add(integButton);
		panel.add(t2);
		panel.add(tf3);
		panel.add(new JLabel());
		panel.add(tf4);
		add(panel);

		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				String str2 = new String();
				str2 = tf2.getText();
				Polynomial p = new Polynomial(str);
				Polynomial p1 = new Polynomial(str2);
				Addition add = new Addition(p, p1);
				tf3.setText(add.result.printPolynomial());
				tf4.setText("");
			}
		});

		subButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				String str2 = new String();
				str2 = tf2.getText();
				Polynomial p = new Polynomial(str);
				Polynomial p1 = new Polynomial(str2);
				Substraction sub = new Substraction(p, p1);
				tf3.setText(sub.result.printPolynomial());
				tf4.setText("");
			}
		});

		multiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				String str2 = new String();
				str2 = tf2.getText();
				Polynomial p = new Polynomial(str);
				Polynomial p1 = new Polynomial(str2);
				Multiplication sub = new Multiplication(p, p1);
				tf3.setText(sub.result.printPolynomial());
				tf4.setText("");
			}
		});

		divButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				String str2 = new String();
				str2 = tf2.getText();
				Polynomial p = new Polynomial(str);
				Polynomial p1 = new Polynomial(str2);
				Division sub = new Division(p, p1);
				tf3.setText(sub.quotient.printPolynomial());
				tf4.setText(sub.remainder.printPolynomial());
			}
		});
		
		derivButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				Polynomial p = new Polynomial(str);
				Derivation sub = new Derivation(p);
				tf3.setText(sub.result.printPolynomial());
				tf4.setText("");
			}
		});
		
		integButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = new String();
				str = tf1.getText();
				Polynomial p = new Polynomial(str);
				Integration sub = new Integration(p);
				tf3.setText(sub.result.printPolynomial());
				tf4.setText("");
			}
		});
		
	}

	public static void main(String[] args) {

		new App();
	}
}
