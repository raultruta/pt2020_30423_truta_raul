package nomen;

import java.util.ArrayList;
import java.util.List;

public class Polynomial {

	public List<Monomial> monomials = new ArrayList<Monomial>();
	public int grade;

	public Polynomial() {
		grade = 0;
	}

	public Polynomial(String input) {
		if (input.isEmpty() == false) {
			String in = new String();
			if (input.startsWith("-")) {
				in = input;
			} else {
				in = "+" + input;
			}
			String[] str = in.split("(?=\\+)|(?=-)");
			for (String string : str) {
				Monomial monom = new Monomial(string);
				monomials.add(monom);
			}
			grade = (int) monomials.get(0).getExponent();
		}
	}

	public String printPolynomial() {
		String polynomialString = new String();
		for (Monomial monom : monomials) {
			if ((int) monom.getCoefficient() >= 0) {
				polynomialString = polynomialString + "+" + (double) monom.getCoefficient() + "x^"
						+ (int) monom.getExponent();
			} else
				polynomialString = polynomialString + (double) monom.getCoefficient() + "x^"
						+ (int) monom.getExponent();
		}
		return polynomialString;
	}

	public void addMonomial(Monomial monom) {
		monomials.add(monom);
	}
}
