package nomen;

public class Monomial {

	private double coefficient;
	private double exponent;

	public Monomial(double x, double y) {
		coefficient = x;
		exponent = y;
	}

	public Monomial(String str) {
		findCoefficient(str);
		findExponent(str);
	}

	private void findCoefficient(String str) {
		int i = 0;
		String aux = new String();
		if (str.toCharArray()[0] == '-') {
			aux = aux + "-";
		}
		while (++i < str.toCharArray().length && str.toCharArray()[i] != 'x') {
			aux = aux + str.toCharArray()[i];
		}
		if (aux.isEmpty() && str.toCharArray()[0] == '+') {
			coefficient = 1;
		} else if ((aux.isEmpty() && str.toCharArray()[0] == '-') || aux.equals("-")) {
			coefficient = -1;
		} else {
			coefficient = new Double(aux);
		}
	}

	private void findExponent(String str) {
		int i = 0;
		while (i < str.toCharArray().length && str.toCharArray()[i] != 'x') {
			i++;
		}
		if (i >= str.toCharArray().length) {
			exponent = 0;
		} else {
			i++;
			String aux = new String();
			while (++i < str.toCharArray().length) {
				aux = aux + str.toCharArray()[i];
			}
			if (aux.isEmpty()) {
				exponent = 1;
			} else {
				exponent = new Double(aux);
			}
		}
	}

	public double getExponent() {
		return this.exponent;
	}

	public double getCoefficient() {
		return this.coefficient;
	}
}
