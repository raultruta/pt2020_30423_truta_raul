package operations;

import nomen.Monomial;
import nomen.Polynomial;

public class Derivation {

	public Polynomial result = new Polynomial();

	public Derivation(Polynomial p1) {

		for (Monomial monom : p1.monomials) {
			if (monom.getExponent() != 0) {
				double nr = monom.getCoefficient() * monom.getExponent();
				double exp = monom.getExponent() - 1;
				Monomial m = new Monomial(nr, exp);
				result.addMonomial(m);
			}
		}
		result.grade = p1.grade - 1;
	}

}
