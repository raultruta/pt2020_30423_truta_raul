package operations;

import nomen.*;

public class Division {

	public Polynomial quotient = new Polynomial();
	public Polynomial remainder = new Polynomial();

	public Division(Polynomial p1, Polynomial p2) {

		if (p2.monomials.isEmpty() == false) {
			if (p2.grade > p1.grade) {
				quotient.addMonomial(new Monomial(0, 0));
				remainder = p1;
			} else {
				remainder = p1;
				while (remainder.grade >= p2.grade) {
					Double nr = remainder.monomials.get(0).getCoefficient() / p2.monomials.get(0).getCoefficient();
					Double exp = p1.monomials.get(0).getExponent() - p2.monomials.get(0).getExponent();
					Monomial auxMonomial = new Monomial(nr, exp);
					quotient.addMonomial(auxMonomial);
					Polynomial auxQuotient = new Polynomial();
					auxQuotient.addMonomial(auxMonomial);
					Multiplication m = new Multiplication(auxQuotient, p2);
					Substraction s = new Substraction(p1, m.result);
					remainder = s.result;
					remainder.grade = (int) remainder.monomials.get(0).getExponent();
				}
			}
		}
	}

}
