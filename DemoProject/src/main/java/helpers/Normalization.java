package helpers;

import nomen.*;

public class Normalization {

	public Polynomial normalPolynomial = new Polynomial();
	
	public Normalization(Polynomial p) {
		
		double nr;
		Monomial monom;
		int i = p.grade;
		while(i >= 0) {
			nr = 0;
			for(Monomial m : p.monomials) {
				if(m.getExponent() == i) {
					nr = nr + m.getCoefficient();
				}
			}
			if(nr != 0) {
				monom = new Monomial(nr, i);
				normalPolynomial.addMonomial(monom);
			}
			i--;
		}
	}
}
